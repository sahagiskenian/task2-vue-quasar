
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('src/pages/Students/StudentPage.vue') },
      { path: 'login', component: () => import('src/pages/Auth/LoginPage.vue') },
      { path: 'register', component: () => import('src/pages/Auth/RegisterPage.vue') },

    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
