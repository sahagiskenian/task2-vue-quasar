export function LoggedIn(state, payload) {
  localStorage.setItem('token', "token");
  state.isLoggedIn = true;
  state.user = payload;
}
export function Register(state, payload) {
  localStorage.setItem('token', "token");
  state.isLoggedIn = true;
  state.user = payload;
}
export function Logout(state) {
  localStorage.removeItem('token');
  state.isLoggedIn = false;
  state.user = '';
}
