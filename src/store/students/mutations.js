export function addToStudents(state, payload) {
  state.students = [...state.students, payload];
}
export function removeFromStudents(state, payload) {

  state.students = state.students.filter(item => item.id !== payload);
}
export function UpdateStudent(state, {objIndex,payload}) {

  if (objIndex>-1) {
      state.students[objIndex].id = payload.id;
      state.students[objIndex].name = payload.name;
      state.students[objIndex].description = payload.description;
      state.students[objIndex].image = payload.image;
  }

}
