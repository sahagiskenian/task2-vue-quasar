export default function () {
  return {
    students: [
      { id: 1, name: "first element", description: 'Dickerson'  },
      { id: 2, name: "sec element", description: 'Larsen'  },
      { id: 3, name: "mid element", description: 'Geneva'  },
      { id: 4, name: "after mid element", description: 'Jami'  }
  ],
  }
}
